package org.droidtr.keyboard;
import android.util.Log;
import java.lang.reflect.Field;
public class ReflectHelper {
	public static Object getDeclaredField(Object o, String field) {
		return getDeclaredField(o.getClass(), o, field);
	}
	public static Object getDeclaredField(Class c, Object o, String field) {
		try {
			Field f = null;
			f = c.getDeclaredField(field);
			f.setAccessible(true);
			return f.get(o);
		} catch (Exception e) {
			Log.e("ReflectHelper", e.getMessage());
			return null;
		}
	}
	public static void setDeclaredField(Object o, String field, Object value) {
		setDeclaredField(o.getClass(), o, field, value);
	}
	public static void setDeclaredField(Class c, Object o, String field, Object value) {
		try {
			Field f = null;
			f = c.getDeclaredField(field);
			f.setAccessible(true);
			f.set(o, value);
		} catch (Exception e) {
			Log.e("ReflectHelper", e.getMessage());
		}
	}
}

