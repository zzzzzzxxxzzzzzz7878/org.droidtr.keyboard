package org.droidtr.keyboard;

import android.content.*;
import android.content.res.*;
import android.graphics.*;
import android.graphics.drawable.*;
import android.os.*;
import android.util.*;
import android.view.*;
import android.view.View.*;
import android.widget.*;

public class emoji {

	Context c;
	View[] tabs;
	LinearLayout emoji =null;
	int selected = 0;
	LinearLayout et, ml;
	View.OnClickListener o;
	boolean start = true;
	LinearLayout.LayoutParams match_parrent = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT, 1);
	String[][] emojis = new String[][]{

			{
			""
		},
			{
			"☺", "👅", "😀", "😁", "😂", "🤣", "😃", "😄", "😅", "😆", "😇",
			"😈", "😉", "😊", "😋", "😌", "😍", "🥰", "😎", "😏", "😐", "😑",
			"😒", "😓", "😔", "😕", "😖", "😗", "😘", "😙", "😚", "😛", "🤗",
			"😜", "😝", "😞", "😟", "😠", "😡", "😢", "😣", "😤", "😥", "🤐",
			"😦", "😧", "😨", "😩", "😪", "😫", "😬", "😭", "😮", "😯", "🤑",
			"😰", "😱", "😲", "😳", "😴", "😵", "😶", "😷", "☝", "✊",
			"✋", "✌", "👆", "👇", "👈", "👉", "👊", "👋", "👌", "👍",
			"👎", "👏", "👐", "🙌", "🙏", "💋", "💌", "💍", "💎", "💏",
			"💐", "💑", "💒", "💓", "💔", "💕", "💖", "💗", "💘", "❤️"
			, "💙", "💚", "💛", "💜", "💝", "💞", "💟", "🏂", "🏃", "🏄",
			"🏇", "🏊", "👤", "👥", "👦", "👧", "👨", "👩", "👪", "👫",
			"👬", "👭", "👮", "👯", "👰", "👱", "👲", "👳", "👴", "👵",
			"👶", "👷", "👸", "👹", "👺", "👻", "👼", "👽", "👾", "👿",
			"💀", "💁", "💂", "💃", "💆", "💇", "💈", "🙅", "🙆", "🙇",
			"🙊", "🙋", "🙍", "🙎", "🚴", "🚵", "🚶"
		}, {
			"©", "®", "‼", "⁉", "™", "ℹ", "Ⓜ", "♻", "⚠", "⚡", "⛔",
			"📵", "🔇", "🔉", "🔊", "🔕", "🔯", "🔱", "🚫", "🚮", "🚯",
			"🚰", "🚱", "🚳", "🚷", "🚸", "🛂", "🛃", "🛄", "🛅",
			"↔", "↕", "↖", "↗", "↘", "↙", "↩", "↪", "⏩", "⏪", "⏫",
			"⏬", "▶", "◀", "➡", "⤴", "⤵", "⬅", "⬆", "⬇", "🔀", "🔁",
			"🔂", "🔃", "🔄", "🔼", "🔽"
		}, {
			"⌚", "⌛", "⏰", "⏳", "☎", "✂", "✅", "✉", "✏", "✒",
			"🌂", "🎒", "🎓", "🎣", "🎤", "🎥", "🎦", "🎧", "🎨", "🎩",
			"🎭", "🎮", "🎰", "🎲", "🎳", "🎴", "🎵", "🎶", "🎷", "🎸",
			"🎹", "🎺", "🎻", "🎼", "🎽", "🎾", "🎿", "🏀", "🏁", "🏆",
			"🏈", "🏉", "🏠", "🏡", "🏧", "🏮", "👑", "👒", "👔", "👕",
			"👗", "👘", "👙", "👚", "👛", "👜", "👝", "👞", "👟", "👠",
			"👡", "👢", "👣", "💄", "💅", "💉", "💊", "💠", "💡", "💢",
			"💣", "💤", "💰", "💲", "💴", "💵", "💺", "💻", "💼", "💽",
			"💾", "💿", "📀", "📖", "📝", "📞", "📠", "📡", "📢", "📣",
			"📨", "📩", "📪", "📫", "📬", "📭", "📮", "📯", "📰", "📱",
			"📲", "📳", "📴", "📶", "📷", "📹", "📺", "📻", "📼", "🔈",
			"🔍", "🔎", "🔏", "🔐", "🔑", "🔒", "🔓", "🔔", "🔞", "🔥",
			"🔦", "🔧", "🔨", "🔩", "🔪", "🔫", "🔬", "🔭", "🔮", "🚀",
			"🚥", "🚦", "🚧", "🚩", "🚬", "🚭", "🚲", "🚹", "🚺", "🚻",
			"🚼", "🚽", "🚾", "🛀"
		}, {
			"▪", "▫", "◻", "◼", "◽", "◾", "☑", "♠", "♣", "♥", "♦",
			"♿", "⚪", "⚫", "⚽", "⚾", "⛄", "⛵", "✔", "✖", "✨",
			"✳", "✴", "❇", "❌", "❎", "❓", "❔", "❕", "❗", "❤",
			"➕", "➖", "➗", "➰", "➿", "⬛", "⬜", "⭐", "⭕", "〰",
			"〽", "㊗", "㊙", "🀄", "🃏", "🎀", "🎁", "🎃", "🎄", "🎅",
			"🎆", "🎇", "🎈", "🎉", "🎊", "🎋", "🎌", "🎍", "🎎", "🎏",
			"🎐", "🎠", "🎡", "🎢", "🎫", "🎬", "🎯", "🎱", "👀", "👂",
			"👃", "👄", "💨", "💩", "🔅", "🔆", "🔋", "🔌", "🔖", "🔗",
			"🔘", "🔲", "🔳", "🔴", "🔵", "🔶", "🔷", "🔸", "🔹", "🔺",
			"🔻", "🚨", "🚪", "🚿", "🛁", "♈", "♉", "♊", "♋", "♌",
			"♍", "♎", "♏", "♐", "♑", "♒", "♓"
		}, {
			"☕", "♨", "🌰", "🍅", "🍆", "🍇", "🍈", "🍉", "🍊", "🍋",
			"🍌", "🍍", "🍎", "🍏", "🍐", "🍑", "🍒", "🍓", "🍔", "🍕",
			"🍖", "🍗", "🍘", "🍙", "🍚", "🍛", "🍜", "🍝", "🍞", "🍟",
			"🍠", "🍡", "🍢", "🍣", "🍤", "🍥", "🍦", "🍧", "🍨", "🍩",
			"🍪", "🍫", "🍬", "🍭", "🍮", "🍯", "🍰", "🍱", "🍲", "🍳",
			"🍴", "🍵", "🍶", "🍷", "🍸", "🍹", "🍺", "🍻", "🍼", "🎂"
		}, {
			"⚓", "⛅", "⛎", "🐋", "👓", "👖", "💥", "💪", "💫", "💬",
			"💭", "💮", "💯", "💱", "💳", "💶", "💷", "💸", "💹", "📁",
			"📂", "📃", "📄", "📅", "📆", "📇", "📈", "📉", "📊", "📋",
			"📌", "📍", "📎", "📏", "📐", "📑", "📒", "📓", "📔", "📕",
			"📗", "📘", "📙", "📚", "📛", "📜", "📟", "📤", "📥", "📦", "📧"
		}, {
			"⛪", "⛲", "⛳", "⛺", "⛽", "🌍", "🌎", "🌏", "🌐", "🎪",
			"🏢", "🏣", "🏤", "🏥", "🏦", "🏨", "🏩", "🏪", "🏫", "🏬",
			"🏭", "🏯", "🏰", "🗻", "🗼", "🗽", "🗾", "🗿", "✈", "🚁",
			"🚂", "🚃", "🚄", "🚅", "🚆", "🚇", "🚈", "🚉", "🚊", "🚋",
			"🚌", "🚍", "🚎", "🚏", "🚐", "🚑", "🚒", "🚓", "🚔", "🚕",
			"🚖", "🚗", "🚘", "🚙", "🚚", "🚛", "🚜", "🚝", "🚞", "🚟",
			"🚠", "🚡", "🚢", "🚣", "🚤"
		}, {
			"🅰", "🅱", "🅾", "🅿", "🆎", "🆑", "🆒", "🆓", "🆔", "🆕", "🆖",
			"🆗", "🆘", "🆙", "🆚", "🔙", "🔚", "🔛", "🔜", "🔝", "🔟",
			"🔠", "🔡", "🔢", "🔣", "🔤", "#⃣", "0⃣", "1⃣", "2⃣", "3⃣",
			"4⃣", "5⃣", "6⃣", "7⃣", "8⃣", "9⃣", "🕐", "🕑", "🕒", "🕓",
			"🕔", "🕕", "🕖", "🕗", "🕘", "🕙", "🕚", "🕛", "🕜", "🕝",
			"🕞", "🕟", "🕠", "🕡", "🕢", "🕣", "🕤", "🕥", "🕦", "🕧",
			"🈁", "🈂", "🈚", "🈯", "🈲", "🈳", "🈴", "🈵", "🈶", "🈷",
			"🈸", "🈹", "🈺", "🉐", "🉑", "🔰"
		}, {
			"🐀", "🐁", "🐂", "🐃", "🐄", "🐅", "🐆", "🐇", "🐈", "🐉",
			"🐊", "🐌", "🐍", "🐎", "🐏", "🐐", "🐑", "🐒", "🐓", "🐔",
			"🐕", "🐖", "🐗", "🐘", "🐙", "🐚", "🐛", "🐜", "🐝", "🐞",
			"🐟", "🐠", "🐡", "🐢", "🐣", "🐤", "🐥", "🐦", "🐧", "🐨",
			"🐩", "🐪", "🐫", "🐬", "🐭", "🐮", "🐯", "🐰", "🐱", "🐲",
			"🐳", "🐴", "🐵", "🐶", "🐷", "🐸", "🐹", "🐺", "🐻", "🐼",
			"🐽", "🐾", "😸", "😹", "😺", "😻", "😼", "😽", "😾", "😿",
			"🙀", "🙈", "🙉", "🌱", "🌲", "🌳", "🌴", "🌵", "🌷", "🌸",
			"🌹", "🌺", "🌻", "🌼", "🌽", "🌾", "🌿", "🍀", "🍁", "🍂",
			"🍃", "🍄", "☀", "☁", "☔", "❄", "🌀", "🌁", "🌃", "🌄",
			"🌅", "🌆", "🌇", "🌈", "🌉", "🌊", "🌋", "🌌", "🌑", "🌒",
			"🌓", "🌔", "🌕", "🌖", "🌗", "🌘", "🌙", "🌚", "🌛", "🌜",
			"🌝", "🌞", "🌟", "🌠", "🎑", "💦", "💧"
		}, {
			"🇹🇷", "🇦🇿", "🇨🇳", "🇩🇪", "🇪🇸", "🇫🇷", "🇬🇧", "🇮🇹", "🇯🇵", "🇰🇷", "🇷🇺",
			"🇺🇸", "🇦🇺", "🇦🇹", "🇧🇪", "🇧🇷", "🇨🇦", "🇨🇱", "🇨🇴", "🇩🇰", "🇫🇮",
			"🇭🇰", "🇮🇳", "🇮🇩", "🇮🇪", "🇮🇱", "🇲🇴", "🇲🇾", "🇲🇽", "🇳🇱", "🇳🇿",
			"🇳🇴", "🇵🇭", "🇵🇱", "🇵🇹", "🇵🇷", "🇸🇦", "🇸🇬", "🇿🇦", "🇸🇪", "🇨🇭",
			"🇦🇪", "🇻🇳", "🏴‍☠️"
		}
	};

	private SharedPreferences read;
	private View.OnLongClickListener clearlast = new OnLongClickListener() {

		@Override
		public boolean onLongClick(View p1) {
			edit.putString("lastemojis", "");
			edit.commit();
			emojis[0] = "".split(" ");
			tabs[0].setVisibility(View.GONE);
			return false;
		}
	};

	SharedPreferences.Editor edit;

	public emoji(Context ctx) {
		c = ctx;
		edit = ctx.getSharedPreferences("key", ctx.MODE_PRIVATE).edit();
		read = ctx.getSharedPreferences("key", ctx.MODE_PRIVATE);

	}

	public LinearLayout getLayout(View.OnClickListener ocl, View.OnClickListener goBack, View.OnClickListener delete) {
		ml = new LinearLayout(c);
		ml.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
		ml.setOrientation(LinearLayout.VERTICAL);
		if (Build.VERSION.SDK_INT >= 14)
		ml.setMotionEventSplittingEnabled(false);
		ml.addView(getEmojiTabs(goBack, delete));
		ml.addView(getEmojiScrollLayout(ocl));
		return ml;
	}

	private View getEmojiTabs(View.OnClickListener goBack, View.OnClickListener delete) {
		if (tabs == null) {
			et = new LinearLayout(c);
			et.setBackgroundColor(Color.rgb(127, 127, 127));
			LinearLayout hll = new LinearLayout(c);
			if (Build.VERSION.SDK_INT >= 14) {
				et.setMotionEventSplittingEnabled(false);
			}
			et.setGravity(Gravity.CENTER);
			et.setLayoutParams(new LinearLayout.LayoutParams(getEmojiWidth(1), -2, 1));
			tabs = new View[emojis.length];
			TextView tv = new TextView(c);
			tv.setLayoutParams(new LinearLayout.LayoutParams(getEmojiWidth(12), getEmojiWidth(12), 0));
			tv.setOnClickListener(goBack);
			tv.setOnLongClickListener(clearlast);
			tv.setTextColor(0xFF000000);
			tv.setTextSize(2 * getEmojiTextSize() / 5);
			tv.setGravity(Gravity.CENTER);
			tv.setText("abc");
			emojis[0] = read.getString("lastemojis", "").split(" ");
			et.addView(tv);
			HorizontalScrollView hsw = new HorizontalScrollView(c);
			hsw.setLayoutParams(new LinearLayout.LayoutParams(getEmojiWidth(1) - getEmojiWidth(6), -2, 1));
			hll.setGravity(Gravity.CENTER);
			for (int i = 0; i < emojis.length; i++) {
				tv = new TextView(c);
				tv.setLayoutParams(new LinearLayout.LayoutParams(getEmojiWidth(12), getEmojiWidth(12), 0));
				tv.setOnClickListener(getEmojiTabOCL);
				tv.setTag(i);
				tv.setTextColor(0xFF000000);
				tv.setTextSize(4 * getEmojiTextSize() / 5);
				tv.setGravity(Gravity.CENTER);
				tv.setText(emojis[i][0]);
				if (i == 0) setSel(tv, true);
				if (!(emojis[i][0].length() > 0)) {
					tv.setVisibility(View.GONE);
				}
				hll.addView(tabs[i] = tv);

			}
			hsw.addView(hll);
			et.addView(hsw);
			tv = new TextView(c);
			tv.setLayoutParams(new LinearLayout.LayoutParams(getEmojiWidth(12), getEmojiWidth(12), 1));
			tv.setOnClickListener(delete);
			tv.setTextColor(0xFF000000);
			Drawable del=c.getResources().getDrawable(R.drawable.delemo);
			del.setColorFilter(Color.BLACK, PorterDuff.Mode.MULTIPLY);
			tv.setBackgroundDrawable(del);
			tv.setGravity(Gravity.CENTER);
			et.addView(tv);
		}
		et.setGravity(Gravity.CENTER);
		return et;
	}

	private View.OnClickListener getEmojiTabOCL =
	new View.OnClickListener() {
		public void onClick(View v) {
			setSelection((int) v.getTag());
		}
	};

	public void setSelection(int index) {
		if (index != selected) {
			for (View x : tabs) setSel(x, false);
			setSel(tabs[index], true);
		}
	}

	private void setSel(View v, boolean b) {
		emojis[0] = read.getString("lastemojis", "").split(" ");

		v.setBackgroundColor(b ? 0xFFFFFF : 0);
		if (b) {
			if (ml.getChildCount() >= 2)
			ml.removeViewAt(1);
			selected = Integer.parseInt(v.getTag().toString());
			if (!start) {
				ml.addView(getEmojiScrollLayout(o));
			} else start = false;
		}
	}

	private LinearLayout getEmojiScrollLayout(View.OnClickListener ocl) {
		LinearLayout ll = new LinearLayout(c);
		LinearLayout llx = new LinearLayout(c);
		ScrollView sw = new ScrollView(c);
		ll.addView(sw);
		sw.addView(llx);
		if (Build.VERSION.SDK_INT >= 14)
		llx.setMotionEventSplittingEnabled(false);
		llx.setOrientation(LinearLayout.VERTICAL);
		LinearLayout[] satirlar = new LinearLayout[(emojis[selected].length / 7) + 1];
		sw.setLayoutParams(new LinearLayout.LayoutParams(-1, (40 * Resources.getSystem().getDisplayMetrics().heightPixels) / 100, 1));
		TextView[] emojiler = new TextView[emojis[selected].length];
		for (int i = 0; i != (emojis[selected].length / 7) + 1; i++) {
			satirlar[i] = new LinearLayout(c);
			if (Build.VERSION.SDK_INT >= 14)
			satirlar[i].setMotionEventSplittingEnabled(false);
			satirlar[i].setOrientation(LinearLayout.HORIZONTAL);
			satirlar[i].setLayoutParams(new LinearLayout.LayoutParams(-1, -1, 1));
			llx.addView(satirlar[i]);
		}
		for (int i = 0; i != emojis[selected].length; i++) {
			emojiler[i] = new TextView(c);
			emojiler[i].setText(emojis[selected][i]);
			emojiler[i].setGravity(Gravity.CENTER);
			emojiler[i].setOnClickListener(o = ocl);
			emojiler[i].setTextSize(getEmojiTextSize());
			emojiler[i].setTextColor(0xFF000000);
			emojiler[i].setSingleLine(true);
			emojiler[i].setLayoutParams(new LinearLayout.LayoutParams(getEmojiWidth(7), -2));
			satirlar[i / 7].addView(emojiler[i]);
		}
		ll.setBackgroundColor(Color.rgb(255, 255, 255));
		return ll;
	}

	public float getDensity() {
		return Resources.getSystem().getDisplayMetrics().density;
	}

	public float getEmojiTextSize() {
		DisplayMetrics dm = Resources.getSystem().getDisplayMetrics();
		return (dm.widthPixels / 7) / (getDensity() * 1.8f);
	}

	public int getEmojiWidth(int oran) {
		return Resources.getSystem().getDisplayMetrics().widthPixels / oran;
	}
	public LinearLayout getEmoji(OnClickListener write,OnClickListener delete,OnClickListener abc){
		emoji=getLayout(write,abc,delete);
		return emoji;
	}
}

