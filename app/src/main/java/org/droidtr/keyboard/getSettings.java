package org.droidtr.keyboard;
import android.content.*;
import android.graphics.*;
import android.graphics.drawable.*;
import android.view.*;
import java.util.*;
import java.io.File;
import android.net.Uri;

public class getSettings extends View{
	//statik değerler
	public static int TRM2=10;
	public static int BF=9;
	public static int MC=8;
	public static int GR=7;
	public static int AR=6;
	public static int RU=5;
	public static int TRM=4;
	public static int TRK=3;
	public static int ENQ=2;
	public static int TRF=1;
	public static int TRQ=0;
	//paylaşımsız değerler
	private SharedPreferences read = null;
	private int keyboard=0;







	private static String toprow="21::x 1,131::f1,¹,½,⅓,¼,⅛ 2,132::f2,²,⅔ 3,133::f3,⅜,³,¾ 4,134::f4,⁴ 5,135::f5,⅝ 6,136::f6,¾ 7,137::f7,⅞ 8,138::f8,∞ 9,139::f9,\uD83C\uDF65 0,140::f10,141::f11,f42::f12,∅,ⁿ 22::x\n";
	private static String bottomsym="abc &v _  / . 66::ent";
	private static String bottomrow="?!# &v  .,? 66::ent";
	private static String bottomsym2="abc &v <  > . 61::tab,19::up,20::down,111::esc,92::pgup,93::pgdn,124::inst,67::del";
	//ayarlar
	public int keyboardBackground;
	public int popupBackground;
	public String rows="";
	public Locale locale=Locale.getDefault();
	public String sym="";
	public String sym2="";
	public int[] primaryButtonColors = null;
	public int[] secondaryButtonColors = null;
	public int heightFactor=40;
	public int[] bg=null;
	public int[] bgdown=null;
	public int[] bgon=null;
	public int defaultButtonRadius;
	public int defaultButtonStroke;
	public int vibration;
	public int defaultButtonColor;
	public int secondaryButtonColor;
	public Drawable keyboardBackgroundDrawable=null;
	public Drawable popupBackgroundDrawable=null;
	public boolean isAudioEffectEnabled=true;
	public boolean isVibrationEnabled=true;
	public boolean isTablet=true;
	public boolean isShiftAvaiable=true;
	public boolean secondRowState=true;
	public boolean butfunc=false;
	public boolean isNavbarEnable=true;
	public int tabViewColor;
	public float secondarySize;
	public float primarySize;
	public int popupDuration;
	public int repeatDuration;
	public int spaceLength;
	public boolean isEmoji;
	public boolean isTabView;
	public boolean otobuyuk=true;
	getSettings(Context c){
		super(c);
		read= c.getSharedPreferences("key", c.MODE_PRIVATE);
		keyboard=read.getInt("keyboardLayout", 0);
		heightFactor=read.getInt("heightFactor",40);
		popupDuration=read.getInt("popupDuration",250);
		repeatDuration=read.getInt("repeatDuration",200);
		primarySize=(float)(read.getInt("textSizeModifier",50)/100f);
		secondarySize=(float)(read.getInt("secondTextSizeModifier",20)/100f);
		vibration=read.getInt("vibtime", 35);
		isVibrationEnabled=read.getBoolean("vibenable", true);
		isAudioEffectEnabled=!read.getBoolean("mute", true);
		keyboardBackground=read.getInt("color",Color.parseColor("#000000"));
		popupBackground=read.getInt("color",Color.parseColor("#000000"));
		defaultButtonColor=read.getInt("butcolor",Color.parseColor("#ffffff"));
		secondaryButtonColor=read.getInt("butseccolor",Color.parseColor("#cccccc"));
		bg=toArray(read.getString("bg","255,0,0,0"));
		bgdown=toArray(read.getString("bgdown","255,0,0,0"));
		bgon=toArray(read.getString("bgon","255,0,0,0"));
		primaryButtonColors=toArray(read.getString("primbut","255,0,0,0"));
		secondaryButtonColors=toArray(read.getString("secondbut","255,0,0,0"));
		butfunc=read.getBoolean("butfunc", true);
		isNavbarEnable=read.getBoolean("navbar", true);
		defaultButtonStroke=read.getInt("strokeWidth",5);
		defaultButtonRadius=read.getInt("radius",22);
		isTablet=read.getBoolean("isTablet",false);
		spaceLength=read.getInt("spaceLength",40);
		isEmoji=read.getBoolean("emoji",true);
		isTabView=read.getBoolean("isTabView",false);
		tabViewColor=read.getInt("tabViewColor",Color.parseColor("#cccccc"));
		otobuyuk=read.getBoolean("otobuyuk",true);
		if(new File(c.getFilesDir() + "/image.png").isFile()){
			keyboardBackgroundDrawable = Drawable.createFromPath(Uri.fromFile(new File(c.getFilesDir() + "/image.png")).getPath());
		}
	}
	public void loadKeys(){
		rows=toprow;
		if(keyboard==1){//1 türkçe f klavye
			locale=new Locale("tr", "TR");
			rows+="f,%,@ g,ğ ğ,g ı,i,¶,į,ì,í,ï,î o,ö,ô,ō,ø,õ,ó,ò,œ d,$,¥ r,® n,!,ñ,ň h,-,° p q,£ w,~\n";
			rows+="u,ü,û,ū,ù,ú,û i,ı,¶,į,ì,í,ï,î e,ə,€,é a,â,ä,á ü,u,û,ū,ù,ú,û t,',₺ k,( m,),µ l,? y,ý,´ ş,s,ß,ś,š,#\n";
			rows+="sft j,+,« ö,o,»,ō,ø,õ,ó,ò,œ v,:,“ c,ç,¢,ć,č ç,c,”,ć,č z,*,ž s,ş,§,ß,ś,š b,;,× x,\",` del\n";
		}else if(keyboard==0){//0 türkçe q klavye
			locale=new Locale("tr", "TR");
			rows+="q,@ w,~ e,ə,€,é r,® t,',₺ y,ý,´ u,ü,û,ū,ù,ú,û ı,i,¶,į,ì,í,ï,î o,ö,ô,ō,ø,õ,ó,ò,œ p,£ ğ,g ü,u,û,ū,ù,ú,û\n";
			rows+="a,â,ä,á s,ş,§,ß,ś,š d,$,¥ f,% g,ğ h,-,° j,+,« k,( l,) ş,s,ß,ś,š,# i,ı,¶,į,ì,í,ï,î\n";
			rows+="sft z,*,ž x,\",` c,ç,¢,ć,č v,:,“ b,;,× n,!,ñ,ň m,?,µ ö,o,»,ō,ø,õ,ó,ò,œ ç,c,”,ć,č del\n";
		} else if(keyboard==2 || keyboard == 4){//2 veya 4 minimal q
			if(keyboard == 2){//2 ingilizce q
				locale=new Locale("en", "US");
			}else if(keyboard == 4){//4 türkçe minik q
				locale=new Locale("tr", "TR");
			}
			rows+="q,@ w,~ e,ə,€,é r,® t,',₺ y,ý,´ u,ü,û,ū,ù,ú,û i,ı,¶,į,ì,í,ï,î o,ö,ô,ō,ø,õ,ó,ò,œ p,£\n";
			rows+="a,â,ä,á s,ş,§,ß,ś,š d,$,¥ f,% g,ğ h,-,° j,+,« k,( l,)\n";
			rows+="sft z,*,ž x,\",` c,ç,¢,ć,č v,:,“ b,;,× n,!,ñ,ň m,?,µ del\n";
		}else if(keyboard==3){//3 köktürkçe
			locale=new Locale("tr", "KT");
			secondRowState=false;
			isShiftAvaiable=false;
			rows+="𐰡 𐰶 𐰨 𐰜 𐰭 𐰸 𐰦 𐰠 𐰞 𐰺 𐰪\n";
			rows+="𐰋 𐰉 𐰏 𐰍 𐰑 𐰓 𐰣 𐰤 𐰯 𐰔 𐰱\n";
			rows+="𐰆 𐰃 𐰀 𐰅 𐱅 𐱃 𐰚 𐰴 𐰢 𐰘 𐱁\n";
			rows+="𐰼 𐰇 𐰲 𐰾 𐰽 𐰖 del\n";
		}else if(keyboard==5){//5 rusça
			locale=new Locale("ru", "RU");
			secondRowState=false;
			rows+="Й ц у к е н г ш щ з х\n";
			rows+="Ф,@ ы,# в,$ а,% п,& р,_ о,/ л,- д,+ ж,( э,)\n";
			rows+="sft я,* ч,\" с,' м,: и,; т,! ь,?,ъ б,< ю,> del\n";
		}else if(keyboard==6){//6 arapça
			locale=new Locale("ar", "AR");
			isShiftAvaiable=false;
			secondRowState=false;
			rows="21::x ١ ٢ ٣ ٤ ٥ ٦ ٧ ٨ ٩ ٠ 22::x\n";
			rows+="ض ص ث ق ف غ ع ه خ ح ج\n";
			rows+="ش س ي ب ل ا ت ن م ك ط\n";
			rows+="ذ ء ؤ ر ى ة و ز ظ د del\n";
		}else if(keyboard==7){//7 yunanca
			locale=new Locale("el", "GR");
			secondRowState=false;
			rows+=";,: ς ε ρ τ υ θ ι ο π\n";
			rows+="α,@,ά σ,# δ,€ φ,% γ,& η,-,ή ξ,+ κ,( λ,)\n";
			rows+="ζ,* χ,\" ψ,' ω,:,ώ β,; ν,! μ,?\n";
		}else if(keyboard==8){//8 morse
			locale=new Locale("C", "C");
			secondRowState=false;
			isShiftAvaiable=false;
			rows+="-\n";
			rows+=".\n";
			rows+="del\n";
		}else if(keyboard==9){//9 brainfuck
			locale=new Locale("C", "C");
			isShiftAvaiable=false;
			secondRowState=false;
			rows+="+ - .\n";
			rows+="[ ] &v\n";
			rows+="< > del\n";
		}else if(keyboard==10){//10 aşırı minimal türkçe q klavye
			locale=new Locale("tr", "TR");
			rows+="e,ə,€,é r,® t,',₺ y,ý,´ i,i,¶,į,ì,í,ï,î,ı u,ü,û,ū,ù,ú,û o,ö,ô,ō,ø,õ,ó,ò,œ p,q,£\n";
			rows+="a,â,ä,á s,ş,§,ß,ś,š d,$,¥ f,% g,ğ h,-,° k,( l,)\n";
			rows+="sft z,*,ž,x,j c,ç,¢,ć,č v,:,“ b,;,× n,!,ñ,ň m,w,?,µ del\n";
		}
		rows+=bottomrow;
		//semboller
		sym=toprow;

		sym+="@ # €,₱,$,¢,£,¥,☭ %,‰ & -,—,_,–,· + (,<,{,[ ),>,},]\n";
		sym+="sft *,★,†,‡ \",„,“,”,«,» ',&v,‘,’‹,› : ; !,¡ ?,¿ del\n";
		sym+=bottomsym;

		sym2="";

		sym2+="21::x ~ ` | •,♣,️♠,️♪,♥️,♦️ √ π,Π ÷ × ¶,§ ∆ 22::x\n";
		sym2+="£ ¥ $,¢ ¢ ^,←,↑,↓,→ °,′,″ =,∞,≠,≈ { }\n";
		sym2+="sft \\ © ® ™ ℅ [ ] del\n";
		sym2+=bottomsym2;
		System.gc();
	}
	public void setKeyboard(int type){
		keyboard=type;
	}
	public int[] toArray(String s){
		String[] ar1=s.split("::");
		int[] ret=new int[ar1.length];
		int i=0;
		while(i<ar1.length){
			String[] ar2=ar1[i].split(",");
			ret[i]=Color.argb(Integer.parseInt(ar2[0]),Integer.parseInt(ar2[1]),Integer.parseInt(ar2[2]),Integer.parseInt(ar2[3]));
			i++;
		}
		return ret;
	}
}
