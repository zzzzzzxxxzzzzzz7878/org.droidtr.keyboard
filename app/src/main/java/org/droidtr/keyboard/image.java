package org.droidtr.keyboard;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

public class image extends Activity {

	private final int SELECT_PHOTO = 1;
	private ImageView imageView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
		photoPickerIntent.setType("image/*");
				startActivityForResult(photoPickerIntent, SELECT_PHOTO);
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
		super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
		Bitmap selectedImage=null;
		switch (requestCode) {
				case SELECT_PHOTO:
				if (resultCode == RESULT_OK) {
					try {
						Uri imageUri = imageReturnedIntent.getData();
						this.setTitle(imageUri.getPath());
						InputStream imageStream = getContentResolver().openInputStream(imageUri);
						selectedImage = BitmapFactory.decodeStream(imageStream);
					} catch (Exception e) {
				}
				saveImageToInternalStorage(selectedImage);
			}
		}
		finish();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}

	public boolean saveImageToInternalStorage(Bitmap image) {
		final SharedPreferences.Editor edit = getSharedPreferences("key", MODE_PRIVATE).edit();
		image = Bitmap.createScaledBitmap(image, getWindowManager().getDefaultDisplay().getWidth(), (getWindowManager().getDefaultDisplay().getWidth()*9)/16, false);
		try {
			FileOutputStream fos = openFileOutput("image.png", Context.MODE_PRIVATE);
			image.compress(Bitmap.CompressFormat.JPEG, 80, fos);
			fos.close();
		} catch (Exception e) {
		}
		int r = 0;
		int b = 0;
		int g = 0;
		int count = 0;
		for (int i = 1; i < image.getHeight() - 10; i++) {
			for (int k = 1; k < image.getWidth() - 10; k++) {
				count = count + 1;
				int j = image.getPixel(k,i);
				r += Color.red(j);
				g += Color.green(j);
				b += Color.blue(j);
			}
		}
		r = r / count;
		g = g / count;
		b = b / count;
		edit.putInt("color", Color.rgb(r, g, b));
		edit.putInt("tabViewColor", Color.argb(110,r, g, b));
		edit.putString("secondbut","120,"+r+","+g+","+b);
		edit.putString("primbut","150,"+r+","+g+","+b);
		edit.putString("bg","100,"+r+","+g+","+b);
		edit.putInt("butseccolor", Color.rgb(255 - r, 255 - g, 255 - b));
		edit.putInt("butcolor", Color.rgb(255 - g, 255 - b, 255 - r));
		edit.commit();
		return false;
	}
}
